// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2024 Stefan Dimitrijevic <stefanstele95@hotmail.com>
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts

import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.plasmoid
import org.kde.kirigami as Kirigami

import org.kde.plasma.private.gamemode

PlasmoidItem {
    id: main

    GamesModel {
        id: gamesModel
    }

    property int count: gamesModel.rowCount
    property bool gamesEmpty: count <= 0

    switchWidth: Kirigami.Units.gridUnit * 5
    switchHeight: Kirigami.Units.gridUnit * 5

    Plasmoid.status: gamesEmpty ? PlasmaCore.Types.PassiveStatus : PlasmaCore.Types.ActiveStatus
    Plasmoid.icon: "input-gamepad-symbolic"
    toolTipMainText: gamesEmpty ? i18nc("@info", "Gamemode inactive") : i18nc("@info", "Gamemode active")
    toolTipSubText: gamesEmpty ? i18nc("@info process count in gamemode", "No active processes") : i18ncp("@info process count in gamemode", "%1 process", "%1 processes", count)
    toolTipTextFormat: Text.PlainText

    fullRepresentation: PlasmaExtras.Representation {
        id: dialogItem

        Layout.minimumHeight: Kirigami.Units.gridUnit * 10
        Layout.minimumWidth: Kirigami.Units.gridUnit * 10
        Layout.preferredHeight: Kirigami.Units.gridUnit * 10
        Layout.preferredWidth: Kirigami.Units.gridUnit * 10

        collapseMarginsHint: true

        contentItem: ListView {
            id: view

            Layout.preferredHeight: Kirigami.Units.gridUnit * 10
            Layout.preferredWidth: Kirigami.Units.gridUnit * 10
            anchors.fill: parent
            model: gamesModel

            delegate: PlasmaExtras.ExpandableListItem {
                icon: "input-gamepad"
                title: executable
                subtitle: i18nc("@label", "PID: %1", String(processid))
            }

            QQC2.Action {
                id: helpAction
                icon.name: "help-contextual"
                text: i18nc("@action open help url", "Help!")
                onTriggered: Qt.openUrlExternally('https://github.com/FeralInteractive/gamemode')
            }

            PlasmaExtras.PlaceholderMessage {
                visible: gamesModel.serviceRegistered && gamesEmpty
                anchors.centerIn: parent
                width: parent.width - (Kirigami.Units.gridUnit * 4)

                text: i18nc("@info", "No Processes in Gamemode")
                    helpfulAction: helpAction
                }

            PlasmaExtras.PlaceholderMessage {
                visible: !gamesModel.serviceRegistered
                anchors.centerIn: parent
                width: parent.width - (Kirigami.Units.gridUnit * 4)

                text: i18nc("@info", "Gamemode Service not Running")
                helpfulAction: helpAction
            }
        }
    }
}
